import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;

import static org.testng.Assert.assertTrue;

public class MyTest {
    @BeforeTest
    public void projectSettings(){
        System.setProperty("webdriver.chrome.driver", "D://Java for AQA//uacomhillel//src//main//resources//chromedriver.exe");

    }

    @Test
    public void secondTest(){
        WebDriver driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        driver.get("https://hotline.ua/login/");
        driver.findElement(By.name("login")).sendKeys("zhmurko.evgen@gmail.com");
        driver.findElement(By.name("password")).sendKeys("Qq123456");
        driver.findElement(By.cssSelector(".btn-graphite.btn-cell")).click();
        WebElement source = driver.findElement(By.xpath("//span[@class='name ellipsis']"));
        Assert.assertEquals("zhmurko.evgen", source.getText());
        assertTrue(driver.findElement(By.xpath("//div[@class='box-in']")).isDisplayed());
        assertTrue(driver.findElement(By.xpath("//div[@class='viewbox cell-sm-none']")).getText().contains("сервис выбора товаров"));
        driver.get("https://hotline.ua/logout");
        driver.close();

    }

}
